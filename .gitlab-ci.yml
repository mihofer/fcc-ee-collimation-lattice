# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Bash.gitlab-ci.yml

# See https://docs.gitlab.com/ee/ci/yaml/README.html for all available options

# you can delete this line if you're not using Docker
image: ubuntu:latest

cache:
  paths:
    - ./codes

stages:
  - build
  - test
  - deploy

before_script:
  - echo "Before script section"
  - apt update -y
  - apt upgrade -y
  - apt install -y wget python3 python3-pip gcc gfortran bison git make patch libx11-dev groff-base
  - python3 -V
  - pip3 install --user matplotlib numpy scipy tfs-pandas generic-parser pytest
  
# BUILD STAGE ------------------------------------------------------------------

get_madx_sad:
  stage: build
  script:
    - echo "Get optics codes"
    - mkdir -p codes 
    - wget -q -O ./codes/madx https://madx.web.cern.ch/madx/releases/5.07.00/madx-linux64-gnu
    - chmod u+x ./codes/madx
    - cd ./codes
    - rm -rf SAD
    - git clone https://github.com/KatsOide/SAD.git
    - cd ./SAD 
    - head -n 70 sad.conf > sad.conf.new && mv -f sad.conf.new sad.conf
    - make depend
    - make -s exe
    - make -s install

# TEST STAGE -------------------------------------------------------------------
  
test_t:
  stage: test
  script:
    - echo "Run python tests on the madx and sad lattices and check if tunes, etc. agree"
    - cd ./scripts
    - ../codes/madx fcc_ee_t.madx > log.mad.tmp
    - ../codes/SAD/bin/gs check_optics_t.sad > log.optics.sad.tmp
    - ../codes/SAD/bin/gs check_da_t.sad > log.da.sad.tmp
    - cd ../tests
    - python3 -m pytest --operation_mode=t
  artifacts:
    when: always
    paths:
      - ./scripts/log.mad.tmp
      - ./scripts/twiss_t_b1_nottapered.tfs
      - ./scripts/aperture_t_b1_nottapered.tfs
      - ./scripts/aperture_t_b1_nottapered_vertical.tfs
      - ./scripts/aperture_t_b1_nottapered_collimator.tfs
      - ./scripts/nottapered_t_b1_thin.seq
      - ./scripts/log.optics.sad.tmp
      - ./scripts/log.da.sad.tmp
      - ./scripts/twiss_t_sad.tfs
      - ./scripts/daxz_t_sad.tfs
      - ./scripts/daxy_t_sad.tfs
    expire_in: 1 week

test_z:
  stage: test
  script:
    - echo "Run python tests on the madx and sad lattices and check if tunes, etc. agree"
    - cd ./scripts
    - ../codes/madx fcc_ee_z.madx > log.mad.tmp
    - ../codes/SAD/bin/gs check_optics_z.sad > log.optics.sad.tmp
    - ../codes/SAD/bin/gs check_da_z.sad > log.da.sad.tmp
    - cd ../tests
    - python3 -m pytest --operation_mode=z
  artifacts:
    when: always
    paths:
      - ./scripts/log.mad.tmp
      - ./scripts/twiss_z_b1_nottapered.tfs
      - ./scripts/aperture_z_b1_nottapered.tfs
      - ./scripts/aperture_z_b1_nottapered_vertical.tfs
      - ./scripts/aperture_z_b1_nottapered_collimator.tfs
      - ./scripts/nottapered_z_b1_thin.seq
      - ./scripts/log.optics.sad.tmp
      - ./scripts/log.da.sad.tmp
      - ./scripts/twiss_z_sad.tfs
      - ./scripts/daxz_z_sad.tfs
      - ./scripts/daxy_z_sad.tfs
    expire_in: 1 week

    
# DEPLOY STAGE -----------------------------------------------------------------

plotting_t:
  stage: deploy
  script:
    - echo "create plots of relevant sections"
    - python3 ./toolkit/plot_lattice.py --tfs_file ./scripts/twiss_t_b1_nottapered.tfs --optics --dispersion --plot_file ./t_lattice_optics.png --reference END
    - python3 ./toolkit/plot_lattice.py --tfs_file ./scripts/twiss_t_b1_nottapered.tfs --optics --dispersion --plot_file ./t_collimation_optics.png --reference END --limits 33000 35500
    - python3 ./toolkit/plot_da.py --da_file ./scripts/daxy_t_sad.tfs --planes XY --plot_file t_da_xy.png --emittance_ratio 0.002
    - python3 ./toolkit/plot_da.py --da_file ./scripts/daxz_t_sad.tfs --planes ZX --plot_file t_da_xz.png --emittance_ratio 0.002
    - python3 ./toolkit/plot_aperture.py --tfs_file ./scripts/aperture_t_b1_nottapered.tfs --beamsize --plot_file t_aperture_all.png --sigma_for_momentum_acceptance 8
    - python3 ./toolkit/plot_aperture.py --tfs_file ./scripts/aperture_t_b1_nottapered.tfs --beamsize --plot_file t_aperture_coll.png --sigma_for_momentum_acceptance 8 --limits 33000 35500
    - python3 ./toolkit/plot_aperture.py --tfs_file ./scripts/aperture_t_b1_nottapered.tfs --beamsize --plot_file t_aperture_ip.png --sigma_for_momentum_acceptance 8 --limits 44000 47000
    - python3 ./toolkit/plot_phasecoverage.py --twiss_file ./scripts/twiss_t_b1_nottapered.tfs --plane X --reference_element TCP.H.B1 --secondary_elements TCS.H[12]. --plot_file t_coverage_x.png
    - python3 ./toolkit/plot_phasecoverage.py --twiss_file ./scripts/twiss_t_b1_nottapered.tfs --plane Y --reference_element TCP.V.B1 --secondary_elements TCS.V[12]. --plot_file t_coverage_y.png  --limit 100
  when: always
  needs:
    ["test_t"]
  dependencies:
    - test_t
  artifacts:
    paths:
      - ./t_lattice_optics.png 
      - ./t_collimation_optics.png 
      - ./t_da_xy.png 
      - ./t_da_xz.png 
      - ./t_aperture_all.png
      - ./t_aperture_coll.png
      - ./t_aperture_ip.png
      - ./t_coverage_x.png
      - ./t_coverage_y.png
    expire_in: 1 week
    
plotting_z:
  stage: deploy
  script:
    - echo "create plots of relevant sections"
    - python3 ./toolkit/plot_lattice.py --tfs_file ./scripts/twiss_z_b1_nottapered.tfs --optics --dispersion --plot_file ./z_lattice_optics.png  --reference END
    - python3 ./toolkit/plot_lattice.py --tfs_file ./scripts/twiss_z_b1_nottapered.tfs --optics --dispersion --plot_file ./z_collimation_optics.png --reference END --limits 33000 35500
    - python3 ./toolkit/plot_da.py --da_file ./scripts/daxy_z_sad.tfs --planes XY --plot_file z_da_xy.png --emittance_ratio 0.0037
    - python3 ./toolkit/plot_da.py --da_file ./scripts/daxz_z_sad.tfs --planes ZX --plot_file z_da_xz.png --emittance_ratio 0.0037
    - python3 ./toolkit/plot_aperture.py --tfs_file ./scripts/aperture_z_b1_nottapered.tfs --beamsize --plot_file z_aperture_all.png --sigma_for_momentum_acceptance 8
    - python3 ./toolkit/plot_aperture.py --tfs_file ./scripts/aperture_z_b1_nottapered.tfs --beamsize --plot_file z_aperture_coll.png --sigma_for_momentum_acceptance 8 --limits 33000 35500
    - python3 ./toolkit/plot_aperture.py --tfs_file ./scripts/aperture_z_b1_nottapered.tfs --beamsize --plot_file z_aperture_ip.png --sigma_for_momentum_acceptance 8 --limits 44000 47000
    - python3 ./toolkit/plot_phasecoverage.py --twiss_file ./scripts/twiss_z_b1_nottapered.tfs --plane X --reference_element TCP.H.B1 --secondary_elements TCS.H[12]. --plot_file z_coverage_x.png
    - python3 ./toolkit/plot_phasecoverage.py --twiss_file ./scripts/twiss_z_b1_nottapered.tfs --plane Y --reference_element TCP.V.B1 --secondary_elements TCS.V[12]. --plot_file z_coverage_y.png --limit 100
  when: always
  needs:
    ["test_z"]
  dependencies:
    - test_z
  artifacts:
    paths:
      - ./z_lattice_optics.png 
      - ./z_collimation_optics.png
      - ./z_da_xy.png 
      - ./z_da_xz.png
      - ./z_aperture_all.png
      - ./z_aperture_coll.png
      - ./z_aperture_ip.png
      - ./z_coverage_x.png
      - ./z_coverage_y.png
    expire_in: 1 week

