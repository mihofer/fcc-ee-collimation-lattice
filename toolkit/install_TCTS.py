import xtrack as xt
import numpy as np
from pathlib import Path
from generic_parser import EntryPointParameters, entrypoint

# Constants --------------------------------------------------------------------

R_BEAMPIPE = 0.03 # m

EMITTANCE = {'H': 0.71e-9,
             'V': 1.9e-12} # m

NSIGMA_TCT = {'H': 13,
              'V': 80}

# TODO: incorporate procedure to find the optimal ds from IP in the script
OPTIMAL_DS_FROM_IP_TCT_H = 690.9613414241976 # m 
OPTIMAL_DS_FROM_IP_TCT_V = 418.1030391121967 # m 

S_IP = {'IPA': 90658.74532762983,
        'IPD': 22664.686329821718,
        'IPG': 45329.372667989664,
        'IPJ': 67994.05899781224}

TCT_NAMES = {'IPA': {'H': 'tct.h.1.b1', 'V': 'tct.v.1.b1'},
             'IPD': {'H': 'tct.h.2.b1', 'V': 'tct.v.2.b1'},
             'IPG': {'H': 'tct.h.3.b1', 'V': 'tct.v.3.b1'},
             'IPJ': {'H': 'tct.h.4.b1', 'V': 'tct.v.4.b1'}}


# Script arguments -------------------------------------------------------------

def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="xtrack_line",
        type=str,
        required=True,
        help="Path to the xtrack line."
    )

    return params


# Entrypoint -------------------------------------------------------------------

@entrypoint(get_params(), strict=True)
def main(opt):
    line = xt.Line.from_json(opt.xtrack_line)

    insert_collimators(line)

    line.build_tracker()
    tw = line.twiss4d()
    line.discard_tracker()

    insert_collimator_apertures(line, tw)

    filename = Path(opt.xtrack_line)
    # Overwrite the original line with the new one with TCTs
    line.to_json(f'{filename.name}')


def insert_collimators(line):
    tct = xt.Drift()

    for IP_name, tct_name in TCT_NAMES.items():
        line.insert_element(tct_name['H'], tct, at_s=S_IP[IP_name] - OPTIMAL_DS_FROM_IP_TCT_H)
        line.insert_element(tct_name['V'], tct, at_s=S_IP[IP_name] - OPTIMAL_DS_FROM_IP_TCT_V)


def insert_collimator_apertures(line):
    for IP_name, tct_name in TCT_NAMES.items():
        tct_aper = xt.LimitEllipse(a=R_BEAMPIPE, b=R_BEAMPIPE)

        line.insert_element(tct_name['H'] + '_aper', tct_aper.copy(), at_s=S_IP[IP_name] - OPTIMAL_DS_FROM_IP_TCT_H)
        line.insert_element(tct_name['V'] + '_aper', tct_aper.copy(), at_s=S_IP[IP_name] - OPTIMAL_DS_FROM_IP_TCT_V)


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()