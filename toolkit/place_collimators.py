"""
Collimator placement
--------------------------------------------------------------------------------

This script determines the optimal position of secondary collimators for given
collimator gaps.
It uses the potential s-coordinate of the primary collimator to evaluate the 
optical functions for a specified twiss, get the optimal phase advance to the
secondary collimators and find the location in the lattice.
The collimator location, aperture, and commands to install it in a MAD-X script
are printed to the commandline or optionally saved to a madx file.

*--Required--*
- **tfs_file** *(str)*: Path to the tfs file. Optics functions have to refer to the end of the element, as is MAD-X standard.
- **primary_position** *(float)*: S-coordinate in meter of the primary collimator.
- **plane** *(str)*: Select in which plane collimator cuts.
- **collimator_opening** *(list)*: Collimator opening in sigma of primary and secondary collimator.

*--Optional--*
- **madx_file** *(str)*: Filename where collimator definitions and sequence edits are saved. If not given they will be printed to the console.
- **secondary_offsets** *(floats)*: Offset for the secondary collimators with respect to the ideal position. List of two values, former corresponding to TCS.1, later to TCS.2.

"""

import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from generic_parser import EntryPointParameters, entrypoint
import tfs
import pandas as pd


# Constants --------------------------------------------------------------------
PLANES=['H', 'V', 'P'] # The P plane is dummy for H when off-momentum collimators are installed
PLANE_TO_COORD={'H': 'X', 'V': 'Y'}
COORD_TO_PLANE={'X': 'H', 'Y': 'V'}
PRIMARY_COLLIMATOR_ABBREV="TCP"
SECONDARY_COLLIMATOR_ABBREV="TCS"


EMITTANCE = {'t': {'H': 1.59e-9, 'V': 1.6E-12},
             'z': {'H': 0.71e-9, 'V': 1.9E-12}}
MOMENTUM_ACCEPTANCE_LIMIT = {'t': 0.028, 'z': 0.013}
NSIGMA_FOR_MOMENTUM_ACCEPTANCE = 0
MOMENTUM_ACCEPTANCE_COLUMN = "Momentum_acceptance"
COLLIMATOR_OPENING = 'Opening'
PLOT_SUFFIX = '.png'

ELEMENTSTYLE = {
    'SOL': {'color': 'yellow'},
    'SEXT': {'color': 'lime'},
    'SEXTUPOLE': {'color': 'lime'},
    'MULT': {'color': 'red'},
    'QUAD': {'color': 'red'},
    'QUADRUPOLE': {'color': 'red'},
    'BEND': {'color': 'midnightblue'},
    'SBEND': {'color': 'midnightblue'},
    'RBEND': {'color': 'midnightblue'},
}

REFER_SIGN = {
    "START":1,
    "CENTER":0,
    "END":-1,
}

# Naming and element definition conventions ------------------------------------
COLLIMATOR_DEFINITION=f"{PRIMARY_COLLIMATOR_ABBREV}: COLLIMATOR, L=0.25;\n"\
                      f"{SECONDARY_COLLIMATOR_ABBREV}: COLLIMATOR, L=0.3;\n"

APERTURE_IN_OTHER_PLANE="0.030"

SEQUENCE='RING'


def collimator_naming(coll_abbrev, plane, specifier):
    return f"{coll_abbrev}.{plane}{specifier}.B1"


def install_element(element_name, element_type, element_position, comment=False):
    return f"\t{'! ' if comment else ''}INSTALL, ELEMENT={element_name}, CLASS={element_type}, AT={element_position:.2f}, FROM=IP.1;\n"


def define_element_aperture(element_name, element_type, operation_mode, plane, beta, cut):
    return f"{element_name}: {element_type}, APERTYPE=RECTANGLE, APERTURE:={{{collimator_aperture(operation_mode, plane, beta, cut)}}};\n"


def collimator_aperture(operation_mode, plane, beta, cut):
    opening= f"{cut}*SQRT({beta:.2f}*{EMITTANCE[operation_mode][plane]})"
    if plane =='H':
        return f"{opening}, {APERTURE_IN_OTHER_PLANE}"
    else:
        return f"{APERTURE_IN_OTHER_PLANE}, {opening}"

# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="operation_mode",
        type=str,
        choices=("z", "t"),
        required=True,
        help="Which FCC-ee operation mode.",
    )
    params.add_parameter(
        name="tfs_file",
        type=str,
        required=True,
        help="Path to the tfs file. Optics functions have to refer to the end of the element, as is MAD-X standard.",
    )
    params.add_parameter(
        name="primary_position",
        type=float,
        required=True,
        help="S-coordinate in meter of the primary collimator.",
    )
    params.add_parameter(
        name="plane",
        type=str,
        required=True,
        choices=PLANES,
        help="Select in which plane collimator cuts.",
    )
    params.add_parameter(
        name="collimator_opening",
        type=float,
        required=True,
        nargs=2,
        help="Collimator opening in sigma of primary and secondary collimator.",
    )
    params.add_parameter(
        name="madx_file",
        type=str,
        help="Filename where collimator definitions and sequence edits are saved. If not given they will be printed to the console.",
    )
    params.add_parameter(
        name="plot_file",
        type=str,
        help="Filename where a plot of the optics, the location of the collimators, momentum acceptance, and collimator gaps will be shown. If not given no plot will be created.",
    )
    params.add_parameter(
        name="secondary_offsets",
        type=float,
        nargs=2,
        default=[0,0],
        help="Offset of the secondary collimators with respect to the ideal position.",
    )
    
    return params


# Entrypoint -------------------------------------------------------------------
@entrypoint(get_params(), strict=True)
def main(opt):
    opt = _check_opts(opt)

    prefix = ''
    if opt.plane == 'P':
        opt.plane = 'H'
        prefix = 'P'

    optics_df = tfs.read(opt.tfs_file)

    if not check_if_element(optics_df, opt.primary_position, "DRIFT"):
        raise ValueError(f"S-location {opt.primary_position:.2f} for primary collimator {collimator_naming(PRIMARY_COLLIMATOR_ABBREV, opt.plane, prefix)} is not a drift.")

    beta_primary, mu_primary = calculate_beta_and_mu(optics_df,
                                                     opt.primary_position,
                                                     PLANE_TO_COORD[opt.plane])

    element_definition=define_element_aperture(collimator_naming(PRIMARY_COLLIMATOR_ABBREV, opt.plane, prefix),
                                               PRIMARY_COLLIMATOR_ABBREV,
                                               opt.operation_mode,
                                               opt.plane,
                                               beta_primary,
                                               opt.collimator_opening[0])

    element_installation=f"SEQEDIT, SEQUENCE={SEQUENCE};\nFLATTEN;\n"
    element_installation+=install_element(collimator_naming(PRIMARY_COLLIMATOR_ABBREV, opt.plane, prefix),
                                          PRIMARY_COLLIMATOR_ABBREV,
                                          opt.primary_position)

    mu=optimal_phase_advance(opt.collimator_opening[0], opt.collimator_opening[1])
    mu_secondaries = [mu_primary + phaseadv for phaseadv in [mu/(2*np.pi), (np.pi-mu)/(2*np.pi)]]
    
    for idx, (mu, offset) in enumerate(zip(mu_secondaries, opt.secondary_offsets)):
        prev_element = (optics_df[optics_df[f"MU{PLANE_TO_COORD[opt.plane]}"] < mu].iloc[-1])
        s=driftlength_from_phaseadvance(prev_element[f"BET{PLANE_TO_COORD[opt.plane]}"],
                                        prev_element[f"ALF{PLANE_TO_COORD[opt.plane]}"],
                                        mu-prev_element[f"MU{PLANE_TO_COORD[opt.plane]}"])
        s=s+offset
        beta_secondary, mu_secondary = calculate_beta_and_mu(optics_df,
                                                             s+prev_element["S"],
                                                             PLANE_TO_COORD[opt.plane])
    
        element_definition+=define_element_aperture(collimator_naming(SECONDARY_COLLIMATOR_ABBREV, opt.plane, f'{prefix}{idx+1}'),
                                                    SECONDARY_COLLIMATOR_ABBREV,
                                                    opt.operation_mode,
                                                    opt.plane,
                                                    beta_secondary,
                                                    opt.collimator_opening[1])
        if not check_if_element(optics_df, s+prev_element["S"], "DRIFT"):
            print(f"Warning: Secondary collimator position {s+prev_element['S']:.2f} for {collimator_naming(SECONDARY_COLLIMATOR_ABBREV, opt.plane, prefix + str(int(idx+1)))} collides with other element {optics_df[optics_df.S > s+prev_element['S']].iloc[0]['NAME']}.")
            print("The element will be included in the installation script, but commented out.")
            element_installation+=install_element(collimator_naming(SECONDARY_COLLIMATOR_ABBREV, opt.plane, f'{prefix}{idx+1}'), SECONDARY_COLLIMATOR_ABBREV, s+prev_element['S'], True)
        else:
            element_installation+=install_element(collimator_naming(SECONDARY_COLLIMATOR_ABBREV, opt.plane, f'{prefix}{idx+1}'), SECONDARY_COLLIMATOR_ABBREV, s+prev_element['S'])
        
    element_installation+=f"FLATTEN;\nENDEDIT;\n"

    if opt.madx_file != None:
        with open(opt.madx_file, 'w') as f:
            f.write(COLLIMATOR_DEFINITION)
            f.write(element_installation)
            f.write(element_definition)
    else:
        print(COLLIMATOR_DEFINITION)
        print(element_definition)
        print(element_installation) 

    if opt.plot_file != None:
        interpol_df = create_interpolated_df_for_drifts(optics_df, PLANE_TO_COORD[opt.plane])

        interpol_df = calc_momentum_acceptance(interpol_df, opt.collimator_opening[0], NSIGMA_FOR_MOMENTUM_ACCEPTANCE, EMITTANCE[opt.operation_mode][opt.plane], MOMENTUM_ACCEPTANCE_COLUMN, PLANE_TO_COORD[opt.plane])
        interpol_df = calc_momentum_acceptance(interpol_df, opt.collimator_opening[1], NSIGMA_FOR_MOMENTUM_ACCEPTANCE, EMITTANCE[opt.operation_mode][opt.plane], f'{MOMENTUM_ACCEPTANCE_COLUMN}_secondary', PLANE_TO_COORD[opt.plane])
        interpol_df = calc_opening(interpol_df, opt.collimator_opening[0], EMITTANCE[opt.operation_mode][opt.plane], f'{COLLIMATOR_OPENING}_primary', PLANE_TO_COORD[opt.plane])
        interpol_df = calc_opening(interpol_df, opt.collimator_opening[1], EMITTANCE[opt.operation_mode][opt.plane], f'{COLLIMATOR_OPENING}_secondary', PLANE_TO_COORD[opt.plane])

        
        primary_collimator = interpol_df.iloc[abs(interpol_df['S'] - opt.primary_position).idxmin()]
        secondary_collimator_1 = interpol_df.iloc[abs(interpol_df[f'MU{PLANE_TO_COORD[opt.plane]}'] - (primary_collimator[f'MU{PLANE_TO_COORD[opt.plane]}'] + mu/(2*np.pi))).idxmin()]
        secondary_collimator_2 = interpol_df.iloc[abs(interpol_df[f'MU{PLANE_TO_COORD[opt.plane]}'] - (primary_collimator[f'MU{PLANE_TO_COORD[opt.plane]}'] + (np.pi-mu)/(2*np.pi))).idxmin()]
        plot_optics_and_collimators(opt.plot_file,
                                    opt.collimator_opening,
                                    optics_df, 
                                    interpol_df, 
                                    opt.operation_mode,
                                    PLANE_TO_COORD[opt.plane],
                                    primary_collimator,
                                    [secondary_collimator_1, secondary_collimator_2])


def _check_opts(opt):
    
    if Path(opt.tfs_file).is_file():    
            opt["tfs_file"]=Path(opt.tfs_file)
    else:
        raise OSError("TFS file path appears to be invalid")
    
    if opt.collimator_opening[0]>opt.collimator_opening[1]:
        raise ValueError("Opening of the secondary can't be smaller than of the primary.")

    opt=_convert_str_to_path(opt, "madx_file")
    opt=_convert_str_to_path(opt, "plot_file")

    if opt.plot_file != None and opt.operation_mode == None:
        raise ValueError("Operation mode needs to be given for plot.")

    return opt


def _convert_str_to_path(opt, file):
    if opt[file]!=None:
            opt[file]=Path(opt[file])
    return opt


def check_if_element(tfs_df, s, elementtype):
    return (tfs_df[tfs_df.S > s].iloc[0]["KEYWORD"] == elementtype)
        

def calculate_beta_and_mu(tfs_df, s, plane):
    prev_element = (tfs_df[tfs_df.S < s].iloc[-1])
    optics_at_prev_element = np.array([prev_element[f"BET{plane}"],
                                       prev_element[f"ALF{plane}"],
                                       (1+prev_element[f"ALF{plane}"]**2)/prev_element[f"BET{plane}"]])
    optics_at_coll = return_propagation_matrix(1, s - prev_element.S, 0, 1)@optics_at_prev_element
    mu_at_coll = phaseadvance_through_drift(optics_at_prev_element[0], optics_at_prev_element[1], s - prev_element.S) + prev_element[f"MU{plane}"]
        
    return optics_at_coll[0], mu_at_coll


def phaseadvance_through_drift(beta0, alpha0, s):
    return np.arctan2(s, beta0-s*alpha0)/(2*np.pi)


def driftlength_from_phaseadvance(beta0, alpha0, mu):
    return np.tan(mu*2*np.pi)*beta0/(1+np.tan(mu*2*np.pi)*alpha0)


def return_propagation_matrix(r11, r12,r21, r22):
    return np.array([[r11**2, -2*r11*r12, r12**2],
                     [-r11*r21, 1+2*r12*r21, -r12*r22],
                     [r21**2, -2*r21*r22, r22**2]])


def create_interpolated_df_for_drifts(df, plane):

    df = df[df['KEYWORD']=='DRIFT']

    interpol_df=pd.DataFrame(data={'S':[],
                                   f'BET{plane}':[],
                                   f'D{plane}':[],
                                   f'MU{plane}':[]})

    for idx, line in df.iterrows():
        s = np.linspace(0, line['L'], 11)        
        beta, disp, mu = calculate_beta_and_disp(line[f'BET{plane}'],
                                                line[f'ALF{plane}'],
                                                line[f'D{plane}'],
                                                line[f'DP{plane}'],
                                                line[f'MU{plane}'],
                                                s)
        interpol_df = interpol_df.append( pd.DataFrame(data={'S': line['S']-s,
                                                             f'BET{plane}':beta,
                                                             f'D{plane}':disp,
                                                             f'MU{plane}':mu}                                                           
                                                       ), ignore_index=True).sort_values('S', ignore_index=True)

    return interpol_df

def calculate_beta_and_disp(beta, alpha, disp, disp_prime, mu, s):
    
    optics_at_end = np.array([beta,
                              alpha,
                              (1+alpha**2)/beta])
    disp_at_end = np.array([disp,
                            disp_prime])
    beta_in_drift = np.array([(np.linalg.inv(return_propagation_matrix(1, l, 0, 1))@optics_at_end)[0] for l in s])
    alfa_in_drift = np.array([(np.linalg.inv(return_propagation_matrix(1, l, 0, 1))@optics_at_end)[1] for l in s])
    disp_in_drift = [(np.linalg.inv(return_orbit_propagation_matrix(1, l, 0, 1))@disp_at_end)[0] for l in s]
    mu_in_drift = [phaseadvance_through_drift(beta_in_drift[idx], -alfa_in_drift[idx], -l)+mu for idx,l in enumerate(s)]

    return beta_in_drift, disp_in_drift, mu_in_drift


def return_orbit_propagation_matrix(C, S,C_prime, S_prime):
    return np.array([[C, S],
                     [C_prime, S_prime]])


def optimal_phase_advance(n1, n2):
    '''
    Formula by Jeanneret, Phys. Rev. ST, 1, 081001 (1998)
    '''
    return np.arctan(np.sqrt(n2**2 - n1**2) / n1)


def optimal_phase_advance_1D(n1, n2):
    '''
    Formula by Trenkler and Jeanneret (1995)
    '''
    return np.arccos(n1 / n2)


def plot_optics_and_collimators(plot_file, collimator_opening, twiss_file, interpol_df, mode, plane, primary_collimator, secondary_collimator):
    
    fig, ax = plt.subplots(nrows=5, ncols=1, figsize=(9,12), gridspec_kw={'height_ratios':[1]+[3]*4})

    fig.suptitle(f'Optics for {mode} operation', fontsize=12)
    
    add_magnets(ax[0], twiss_file)

    ax[1].plot(interpol_df['S'], interpol_df[f'BET{plane}'], linewidth=2, color='red', label='Interpolation')
    ax[1].plot(twiss_file['S'], twiss_file[f'BET{plane}'], linewidth=2, color='red', linestyle='--',label='Twiss')
    ax[1].legend(fontsize=12, loc='upper left', bbox_to_anchor=(1.01, 1.0), ncol=1)
    # ax[1].set_ylabel(f"$\beta_{plane}$ [m]", fontsize=12)
    ax[1].tick_params(axis='both', which='major', labelsize=12)
    ax[1].set_ylim([0, interpol_df[f'BET{plane}'].max()*1.2])
    shade_magnets(ax[1], twiss_file)    
    add_coll_line(ax[1], primary_collimator, secondary_collimator[0], f"BET{plane}", 1.1)
    add_coll_line(ax[1], primary_collimator, secondary_collimator[1], f"BET{plane}", 0.9)


    ax[2].plot(interpol_df['S'], interpol_df[f'D{plane}'], linewidth=2, color='lime', label='Interpolation')
    ax[2].plot(twiss_file['S'], twiss_file[f'D{plane}'], linewidth=2, color='lime', linestyle='--',label='Twiss')
    ax[2].legend(fontsize=12, loc='upper left', bbox_to_anchor=(1.01, 1.0), ncol=1)
    ax[2].set_ylabel(f"$D_{plane}$ [m]", fontsize=12)
    ax[2].tick_params(axis='both', which='major', labelsize=12)
    ax[2].set_ylim([interpol_df[f'D{plane}'].min()*1.1, interpol_df[f'D{plane}'].max()*1.2])
    shade_magnets(ax[2], twiss_file)


    ax[3].plot(interpol_df['S'], interpol_df[MOMENTUM_ACCEPTANCE_COLUMN], linewidth=2, color='blue', label=f'primary collimator \nwith opening {collimator_opening[0]} sigma_x')
    ax[3].plot(interpol_df['S'], interpol_df[f'{MOMENTUM_ACCEPTANCE_COLUMN}_secondary'], linewidth=2, color='blue', linestyle='--', label=f'secondary collimator \nwith opening {collimator_opening[1]} sigma_x')
    ax[3].axhline(y=MOMENTUM_ACCEPTANCE_LIMIT[mode], label='min. Momentum acceptance', color='grey')

    ax[3].set_ylabel("Momentum acceptance", fontsize=12)
    ax[3].tick_params(axis='both', which='major', labelsize=12)
    
    add_coll_line(ax[3], primary_collimator, secondary_collimator[0], MOMENTUM_ACCEPTANCE_COLUMN, 1.1)
    add_coll_line(ax[3], primary_collimator, secondary_collimator[1], MOMENTUM_ACCEPTANCE_COLUMN, 0.9)
    shade_magnets(ax[3], twiss_file)

    ax[3].legend(fontsize=12, loc='upper left', bbox_to_anchor=(1.01, 1.0), ncol=1)


    ax[4].plot(interpol_df['S'], interpol_df[f'{COLLIMATOR_OPENING}_primary'], linewidth=2, color='grey', label='Primary')  
    ax[4].plot(interpol_df['S'], interpol_df[f'{COLLIMATOR_OPENING}_secondary'], linewidth=2, color='grey', linestyle='--', label='Secondary')  
    add_coll_line(ax[4], primary_collimator, secondary_collimator[0], f'{COLLIMATOR_OPENING}_primary', 1.1)
    add_coll_line(ax[4], primary_collimator, secondary_collimator[1], f'{COLLIMATOR_OPENING}_primary', 0.9)
    
    
    ax[4].set_ylabel("Collimator \n aperture [mm]", fontsize=12)
    ax[4].set_ylim([0, collimator_opening[1]*np.sqrt(interpol_df[f'BET{plane}'].max()*EMITTANCE[mode][COORD_TO_PLANE[plane]])*1000*1.3])
    ax[4].tick_params(axis='both', which='major', labelsize=12)   
    ax[4].legend(fontsize=12, loc='upper left', bbox_to_anchor=(1.01, 1.0), ncol=1)
    ax[4].set_xlabel("s [m]", fontsize=12) 
    
    plt.savefig(plot_file.with_suffix(PLOT_SUFFIX))
    plt.show()
    
    return fig, ax


def add_magnets(ax, data, refer='END'):

    ax.axhline(color='black')
    ax.yaxis.set_visible(False)
    ax.xaxis.set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.set_ylim([-1.2, 1.2])
    
    for idx, elem in data.iterrows():
        try:
            ax.bar(elem['S']+REFER_SIGN[refer]*elem['L']/2.,
                   get_height(elem),
                   elem['L'],
                   bottom=get_bottom(elem),
                   color=ELEMENTSTYLE[str(elem['KEYWORD'])]['color'],
                   alpha=0.8)
        except KeyError:
            pass
    

def get_height(elem):
    if str(elem['KEYWORD']) == 'QUAD' or str(elem['KEYWORD']) == 'QUADRUPOLE':
        return np.sign(elem['K1L'])
    if str(elem['KEYWORD']) == 'MULT':
        return np.sign(elem['K1L'])
    elif str(elem['KEYWORD']) == 'BEND' or str(elem['KEYWORD']) == 'SBEND' or str(elem['KEYWORD']) == 'RBEND':
        return 1.
    elif str(elem['KEYWORD']) == 'SEXT' or str(elem['KEYWORD']) == 'SEXTUPOLE':
        return np.sign(elem['K2L'])*0.75
    else:
        return 0.


def get_bottom(elem):
    if str(elem['KEYWORD']) == 'QUAD' or str(elem['KEYWORD']) == 'QUADRUPOLE':
        return 0.
    elif str(elem['KEYWORD']) == 'MULT':
        return 0.
    elif str(elem['KEYWORD']) == 'BEND' or str(elem['KEYWORD']) == 'SBEND' or str(elem['KEYWORD']) == 'RBEND':
        return -0.5
    elif str(elem['KEYWORD']) == 'SEXT' or str(elem['KEYWORD']) == 'SEXTUPOLE':
        return 0.
    else:
        return 0.


def shade_magnets(ax, df):
    df= df[df['KEYWORD']!='DRIFT' ]
    
    for _, line in df.iterrows():
        ax.fill_betweenx( ax.get_ylim(), line['S']-line['L'], line['S'], color='black', alpha=0.1)


def calc_momentum_acceptance(df, coll_opening, n_sigma, emit, column_name, plane):

    df[column_name] = (coll_opening - n_sigma)*np.sqrt(df[f'BET{plane}']*emit)/ abs(df[f'D{plane}'])

    return df


def calc_opening(df, coll_opening, emit, column_name, plane):

    df[column_name] = coll_opening*np.sqrt(df[f'BET{plane}']*emit)*1000

    return df


def add_coll_line(ax, df1, df2, y_column, factor=1):
    plotdistance = ax.get_xlim()[1]-ax.get_xlim()[0]
    ax.axhline(y=df1[y_column]*factor, xmin=(df1['S']-ax.get_xlim()[0])/plotdistance, xmax=(df2['S']-ax.get_xlim()[0])/plotdistance, color='black', alpha=1., marker='X')

# Script Mode ------------------------------------------------------------------


if __name__ == "__main__":
    main()
